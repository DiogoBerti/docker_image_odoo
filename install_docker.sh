sudo apt-get update
sudo apt-get install curl
curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh
sudo docker -v
sudo usermod -aG docker $USER
docker -v

sudo curl -L https://github.com/docker/compose/releases/download/1.19.0-rc3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose -v

git clone https://github.com/odoo/odoo.git -b 11.0 --depth=300 odoo
git clone https://github.com/Trust-Code/odoo-brasil.git -b 11.0 odoo-brasil
 